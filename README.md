# Campus Contest 2021

Campus contest 2021 - realized for Campus Academy Rennes

# Decription du projet
L’objectif au sein de ce projet est de réaliser une application web où le visiteur pourra jouer contre l’ordinateur.
Dans un premier temps le joueur se trouve sur une page d'accueil. 
Grâce à cette page il peut soit lire les règles, soit commencer une partie.
Quand il commence une partie, le joueur doit choisir quel coup il va jouer.
Après validation, l'ordinateur choisira son coup.
Nous saurons donc qui est le gagnant.
Le joueur peut rejouer autant de fois qu'il veut.

# Participants

GAYOT Josselin - SWITCH IT - B1
PONTAIS Gwladys - SWITCH IT - B1
VALANTE Thomas - SWITCH IT - B1