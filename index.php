<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Shifumi</title>
  <link rel="stylesheet" href="./assets/css/style_acceuil.css">
</head>

<body>
  <img src="./assets/img/shifumi.png" alt="ShifumiImage" class="Titre">
  <div class="tout">


  <div class= "BoutonJouer">
      <form action='pageJouer.php' method='POST' name='formulaire'>
          <input type='image' style="width: 80%;" src='./assets/img/jouer.png'>
      </form>
  </div>

<div class="RockPaperScissorsImages">
    <div id="test" class="rotating">
      <img src="./assets/img/rock.png" alt="RockImage" class="Rotating">
    </div>
    <div id="test" class="rotating">
      <img src="./assets/img/paper.png" alt="PaperImage" class="Rotating">
    </div>
    <div id="test" class="rotating">
      <img src="./assets/img/scissors.png" alt="ScissorsImage" class="Rotating">
    </div>
</div>

<div class= "BoutonRegles">
    <form action='pageRegles.php' method='POST' name='formulaire'>
        <input type='image' style="width: 80%;" src='./assets/img/regles.png'>
    </form>
</div>
  </div>
</body>

<script src="assets/js/script.js"></script>

</html>
