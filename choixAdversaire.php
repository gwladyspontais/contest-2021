<!DOCTYPE html>
<html lang="fr">

    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shifumi</title>
    <link rel="stylesheet" href="assets/css/style_jouer.css">
    </head>

    <body>
        <div class="container">
            <?php
            $choixJoueur = $_POST['choix'];
            $choixPossible = ['papier', 'pierre', 'ciseaux'];
            
            shuffle($choixPossible);
            $choixAdversaire = $choixPossible[1];

            echo "Joueur :";
            if ($choixJoueur=='papier'){
                echo"<img src='./assets/img/paper.png' height='20%' width='20%' img/> ";
            }
            elseif ($choixJoueur=='pierre'){
                echo"<img src='./assets/img/rock.png' height='20%' width='20%' img/> ";
            }
            elseif ($choixJoueur=='ciseaux'){
                echo"<img src='./assets/img/scissors.png' height='20%' width='20%' img/> ";
            }
            
            echo "Ordinateur : ";
            if ($choixAdversaire=='papier'){
                echo"<img src='./assets/img/paperBOT.png' height='20%' width='20%' img/>";
            }
            elseif ($choixAdversaire=='pierre'){
                echo"<img src='./assets/img/rockBOT.png' height='20%' width='20%' img/>";
            }
            elseif ($choixAdversaire=='ciseaux'){
                echo"<img src='./assets/img/scissorsBOT.png' height='20%' width='20%' img/>";
            }

        
             echo "<br><br> Résultat : <br>"; 

            if ($choixJoueur == $choixAdversaire){
                echo 'Match nul <br><br>';
                
            }
            elseif($choixJoueur =='papier' && $choixAdversaire =='pierre'){
                echo 'Vous avez gagné <br><br>';
            }
            elseif($choixJoueur =='papier' && $choixAdversaire =='ciseaux'){
                echo 'Vous avez perdu <br><br>';
            }
            elseif($choixJoueur =='pierre' && $choixAdversaire =='papier'){
                echo 'Vous avez perdu <br><br>';
            }
            elseif($choixJoueur =='pierre' && $choixAdversaire =='ciseaux'){
                echo 'Vous avez gagné <br><br>';
            }
            elseif($choixJoueur =='ciseaux' && $choixAdversaire =='pierre'){
                echo 'Vous avez perdu <br><br>';
            }
            elseif($choixJoueur =='ciseaux' && $choixAdversaire =='papier'){
                echo 'Vous avez gagné <br><br>';
            }
            ?>
        </div>
        <div class= "bouton">
        <form action='pageJouer.php' method='POST' name='formulaire'>
                <input type='image' src='./assets/img/rejouer.png' height='40%' width='40%'>
            </form>
        </div>
    </body>

    <script src="assets/js/script.js"></script>

</html>