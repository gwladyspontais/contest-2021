<!doctype html>

<html lang="fr">

  <head>
    <meta charset="utf-8">
    <title>Règles du shifumi</title>
    <link rel="stylesheet" href="./assets/css/style_regles.css">
  </head>

  <body>

  <div class= "regles" <p> Le shifumi est un jeu d’origine chinoise.</p></div>
  <div class= "regles2" <p>A chaque partie, le joueur choisit l’une des trois actions suivantes :</p> </div>
  <div class= "choix" <ul>
    <li> la pierre </li>
    <li> les ciseaux </li>
    <li> la feuille </li>
  </ul> </div>

  <div class= "regles3" 
    <p> De façon générale, la pierre bat les ciseaux (en les émoussant), les
      ciseaux battent la feuille (en la coupant), la feuille bat la pierre (en
      l’enveloppant).<br>
      Ainsi chaque coup bat un autre coup, fait match nul contre son homologue et est battu par la troisième option.
    </p>
  </div>

  <div class= "bouton">
    <form action='index.php' method='POST'>
      <input type='image' src='./assets/img/retour.png' height='20%' width='20%'>
    </form>
  </div>

  <div class= "bouton2">
    <form action='pageJouer' method='POST'>
      <input type='image' src='./assets/img/jouer.png' height='25%' width='25%'>
    </form>
  </div>
  </body>

</html>
